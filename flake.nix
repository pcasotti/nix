{
    description = "A very basic flake";

    inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

        nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

        home-manager = {
            url = "github:nix-community/home-manager/release-24.11";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        impermanence.url = "github:nix-community/impermanence";

        nixos-cosmic = {
            url = "github:lilyinstarlight/nixos-cosmic";
            inputs.nixpkgs.follows = "nixpkgs-unstable";
        };

        catppuccin = {
            url = "github:catppuccin/nix";
            inputs.nixpkgs.follows = "nixpkgs-unstable";
        };
    };

    outputs = {
        self,
        nixpkgs,
        nixpkgs-unstable,
        home-manager,
        impermanence,
        nixos-cosmic,
        catppuccin,
    } @ inputs:
    let
        system = "x86_64-linux";
        unstable = import nixpkgs-unstable { inherit system; config = { allowUnfree = true; }; };
        pkgs = import nixpkgs { inherit system; config = { allowUnfree = true; }; };
    in
    {
        nixosConfigurations.box = nixpkgs.lib.nixosSystem {
            inherit system;
            specialArgs = { inherit inputs; };
            modules = [
                {
                    nix.settings = {
                        substituters = [ "https://cosmic.cachix.org/" ];
                        trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=" ];
                    };
                }

                catppuccin.nixosModules.catppuccin
                nixos-cosmic.nixosModules.default
                impermanence.nixosModules.impermanence

                ./nixos/configuration.nix

                home-manager.nixosModules.home-manager {
                    home-manager.useGlobalPkgs = true;
                    home-manager.useUserPackages = true;
                    home-manager.backupFileExtension = "backup";
                    home-manager.users.pedro = import home-manager/home.nix;
                    home-manager.extraSpecialArgs = { inherit inputs system unstable; };
                }
            ];
        };
    };
}
