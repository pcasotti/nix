{ pkgs, ... }:
{
  home.packages = with pkgs; [
    (writeShellScriptBin "gc" ''
      sudo nix-collect-garbage -d
      sudo nix-store --optimise
    '')

    (writeShellScriptBin "nixupdate" ''
      pushd ~/nix
      nix flake update
      popd
    '')

    (writeShellScriptBin "nixswitch" ''
      pushd ~/nix
      sudo nixos-rebuild switch --flake .?submodules=1#box --show-trace
      popd
    '')

    (writeShellScriptBin "nixupgrade" ''
      nixupdate
      nixswitch
    '')
  ];
}
