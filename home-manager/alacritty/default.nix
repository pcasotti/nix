{
  programs.alacritty = {
    enable = true;
    settings = {
      env.TERM = "xterm-256color";
      window = {
        opacity = 0.98;
        dynamic_title = true;
      };
      font.size = 12.0;
    };
  };
}
