{ pkgs, config, lib, unstable, ... }:
let
  inherit (unstable.vscode-utils) buildVscodeMarketplaceExtension;
  java-test = buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "vscode-java-test";
      publisher = "vscjava";
      version = "0.41.1";
      hash = "";
    };
    vsix = unstable.fetchurl {
      url = "https://open-vsx.org/api/vscjava/vscode-java-test/0.41.1/file/vscjava.vscode-java-test-0.41.1.vsix";
      hash = "sha256-iXIeTkyn1LNtG8Fs9pIw56dFgFfPZy5nQWRPxBHoZMI=";
      name = "vscode-java-test.zip";
    };
  };
in
rec {
  programs.neovim = {
    enable = true;

    vimAlias = true;
    vimdiffAlias = true;
    defaultEditor = true;

    package = unstable.neovim-unwrapped;

    extraPackages = with unstable; [
      rust-analyzer
      #graphviz
      lldb
      vscode-extensions.vadimcn.vscode-lldb
      cargo

      jdt-language-server
      vscode-extensions.vscjava.vscode-java-debug
      java-test
      jdk17
      (pkgs.python3.withPackages (python-pkgs: [
        python-pkgs.python-lsp-server
        python-pkgs.jedi
        python-pkgs.jedi-language-server
        python-pkgs.rope
        python-pkgs.pyflakes
        python-pkgs.mccabe
        python-pkgs.pycodestyle
        python-pkgs.pydocstyle
        python-pkgs.yapf
        python-pkgs.flake8
        python-pkgs.pylint
        python-pkgs.pylsp-rope
        python-pkgs.pylsp-mypy
        python-pkgs.python-lsp-ruff
        python-pkgs.debugpy
      ]))

      #llvmPackages_latest.lldb
      #llvmPackages_latest.libstdcxxClang
      llvmPackages_latest.libcxxClang
      llvmPackages_latest.libcxxStdenv
      #llvmPackages_latest.libcxxabi
      llvmPackages_latest.libcxx
      llvmPackages_latest.libllvm
      #libstdcxx5
      clang-tools

      pyright
      pylyzer
      # python312Packages.python-lsp-server
      # python312Packages.jedi
      # python312Packages.jedi-language-server
      # python312Packages.rope
      # python312Packages.pyflakes
      # python312Packages.mccabe
      # python312Packages.pycodestyle
      # python312Packages.pydocstyle
      # python312Packages.yapf
      # python312Packages.flake8
      # python312Packages.pylint
      #
      # python312Packages.pylsp-rope
      # python312Packages.pylsp-mypy
      # python312Packages.python-lsp-ruff
      # ruff
      # mypy

      #vhdl-ls

      lua-language-server

      nil
      nixpkgs-fmt

      wl-clipboard

      luajitPackages.jsregexp

      glslls

      ghc
      haskell-language-server
      cabal-install

      tree-sitter
    ];

    #plugins = [ pkgs.vimPlugins.nvim-treesitter.withAllGrammars ];

    #extraLuaConfig = builtins.readFile ../../dotfiles/.config/nvim/init.lua;

    extraWrapperArgs = [
      "--suffix" "LIBRARY_PATH" ":" "${lib.makeLibraryPath [ unstable.stdenv.cc.cc unstable.zlib unstable.clangStdenv.cc ]}"
      "--suffix" "PKG_CONFIG_PATH" ":" "${lib.makeSearchPathOutput "dev" "lib/pkgconfig" [ unstable.stdenv.cc.cc unstable.zlib ]}"
      "--suffix" "JDTLS_PATH" ":" "${unstable.jdt-language-server}/bin/jdtls"
      "--suffix" "JDTLS_DEBUG" ":" "${unstable.vscode-extensions.vscjava.vscode-java-debug}/share/vscode/extensions/vscjava.vscode-java-debug/server/com.microsoft.java.debug.plugin-*.jar"
      "--suffix" "JDTLS_TEST" ":" "${java-test}/share/vscode/extensions/vscjava.vscode-java-test/server/*.jar"
      "--suffix" "CODELLDB" ":" "${unstable.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/adapter/codelldb"
      "--suffix" "LIBLLDB" ":" "${unstable.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/lldb/lib/liblldb.so"
      "--suffix" "CPPTOOLS_DAP" ":" "${unstable.vscode-extensions.ms-vscode.cpptools}/share/vscode/extensions/ms-vscode.cpptools/debugAdapters/bin/OpenDebugAD7"
    ];
  };

  home.packages = programs.neovim.extraPackages;

  xdg.configFile."nvim/init.lua".enable = false;
  home.file = {
    # ".config/nvim/init.lua" = { source = ../../dotfiles/.config/nvim/init.lua; recursive = true; };
    # ".config/nvim/ftplugin" = { source = ../../dotfiles/.config/nvim/ftplugin; recursive = true; };
    # ".config/nvim/lua/lsp.lua" = { source = ../../dotfiles/.config/nvim/lua/lsp.lua; };
    # ".config/nvim/lua/plugins" = { source = ../../dotfiles/.config/nvim/lua/plugins; recursive = true; };
    # ".config/nvim/lazy-lock.json" = { source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/nix/dotfiles/.config/nvim/lazy-lock.json"; recursive = true; };
    ".config/nvim" = { source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/nix/dotfiles/.config/nvim"; recursive = true; };

    # ".config/nvim/lua/paths.lua".text = ''
    #   local M = {}
    #   M.jdtls = {}
    #   M.jdtls.jdtls = '${pkgs.jdt-language-server}/bin/jdtls'
    #   M.jdtls.debug = '${pkgs.vscode-extensions.vscjava.vscode-java-debug}/share/vscode/extensions/vscjava.vscode-java-debug/server/com.microsoft.java.debug.plugin-*.jar'
    #   M.jdtls.test = '${pkgs.vscode-extensions.vscjava.vscode-java-test}/share/vscode/extensions/vscjava.vscode-java-test/server/*.jar'
    #   M.codelldb = {}
    #   M.codelldb.codelldb = '${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/adapter/codelldb'
    #   M.codelldb.liblldb = '${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/lldb/lib/liblldb.so'
    #   M.cpptools_dap = '${pkgs.vscode-extensions.ms-vscode.cpptools}/share/vscode/extensions/ms-vscode.cpptools/debugAdapters/bin/OpenDebugAD7'
    #   return M
    # '';
  };
}
