{ pkgs, ... }:
{
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    keyMode = "vi";
    mouse = true;
    plugins = with pkgs.tmuxPlugins; [
      sensible
      vim-tmux-navigator
      yank
      {
        plugin = resurrect;
        extraConfig = ''
          set -g @resurrect-strategy-nvim 'session'
          set -g @resurrect-capture-pane-contents 'on'
        '';
      }
      {
        plugin = continuum;
        extraConfig = "set -g @continuum-restore 'on'";
      }
    ];
    prefix = "c-space";
    sensibleOnTop = true;
    extraConfig = ''
      set-option -sa terminal-overrides ",xterm*:Tc"

      bind -n m-h previous-window
      bind -n m-l next-window
      bind -n m-s copy-mode
      bind -n m-f resize-pane -Z
      bind -n m-x kill-pane
      bind -n m-v split-window -v -c "#{pane_current_path}"
      bind -n m-n split-window -h -c "#{pane_current_path}"
      bind -n m-t new-window -c "#{pane_current_path}"

      set-window-option -g pane-base-index 1
      set-option -g renumber-windows on
      set-option -g set-titles on

      bind-key -T copy-mode-vi v send-keys -X begin-selection
      bind-key -T copy-mode-vi c-v send-keys -X rectangle-toggle
      bind-key -T copy-mode-vi y send-keys -X copy-selection
    '';

  };
}

