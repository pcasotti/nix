{
  programs.helix = {
    enable = true;
    # defaultEditor = true;

    settings = {
      # theme = "catppuccin_mocha";
      editor = {
        line-number = "relative";
        cursorline = true;
        rulers = [100];
        cursor-shape.insert = "bar";
        indent-guides.render = true;
      };
    };

    languages.language-server = {
      jdtls = {
        config = {
          maven.userSettings = "/home/pedro/dev/labsec/validador-de-documentos/settings.xml";
          configuration.maven.userSettings = "/home/pedro/dev/labsec/validador-de-documentos/settings.xml";
          java.configuration.maven.userSettings = "/home/pedro/dev/labsec/validador-de-documentos/settings.xml";
          settings.java.configuration.maven.userSettings = "/home/pedro/dev/labsec/validador-de-documentos/settings.xml";
        };
      };
    };
  };
}
