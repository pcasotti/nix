# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)

{ inputs, lib, config, pkgs, system, unstable, ... }:
{
  # You can import other home-manager modules here
  imports = [
    # If you want to use home-manager modules from other flakes (such as nix-colors):
    # inputs.nix-colors.homeManagerModule

    # You can also split up your configuration and import pieces of it here:
    ./scripts.nix

    ./alacritty
    ./bat
    ./bottom
    ./direnv
    ./eza
    ./firefox
    ./fontconfig
    ./fuzzel
    ./fzf
    ./git
    ./gitui
    ./helix
    ./jq
    ./kitty
    ./lazygit
    # ./nixvim
    ./nvim
    ./ripgrep
    ./rofi
    ./thunderbird
    ./tmux

    inputs.catppuccin.homeManagerModules.catppuccin
    # inputs.nixvim.homeManagerModules.nixvim
  ];

  catppuccin.flavor = "mocha";
  catppuccin.enable = true;

  nixpkgs = {
    # You can add overlays here
    # overlays = [
      # If you want to use overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    # ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = (_: true);
    };
  };

  home = {
    username = "pedro";
    homeDirectory = "/home/pedro";
  };

  home.packages = with pkgs; [
    eww
    kicad-small # Currently broken in unstable
    ncdu
    fd
    steam
    heroic
    discord
    macchina
    xorg.xmodmap
    libsForQt5.qt5ct
    # obsidian
    krita
    qbittorrent
    unrar
    bottles
    lutris
    wineWowPackages.staging
    tree
    libnotify
    kdenlive
    steam-run
    wirelesstools
    socat
    sshfs
    yazi
    vesktop
    nurl
    gamescope
    mangohud
    virt-manager
    nwg-look

    dunst
    pipewire
    wireplumber
    libsForQt5.qt5.qtwayland
    qt6.qtwayland
    libsForQt5.polkit-kde-agent
    hyprpaper
    hyprpicker
    brightnessctl
    pamixer
    libsForQt5.qtstyleplugin-kvantum
    grim
    slurp
    swaybg
    man-pages
    man-pages-posix
    gnumake
    lsof
    mgba
    glow
    gtklock
    postman
    usbutils
    blender
    postman
    obs-studio
    mpv
    ffmpeg
    pavucontrol
    (mars-mips.overrideAttrs (oldAttrs: {
      src = pkgs.fetchurl {
        url = "https://github.com/aeris170/MARS-Theme-Engine/releases/download/v4.5.1.1/Mars4_5_1_1Unofficial.jar";
        sha256 = "MOd9ZjJuilW7P4OjddH/aTgs4CJUYVm3rMDlsiUqr9s=";
      };
    }))

    # (let base = pkgs.appimageTools.defaultFhsEnvArgs; in pkgs.buildFHSUserEnv rec {
    #   name = "fhs";
    #   targetPkgs = pkgs: ([
    #     (base.targetPkgs pkgs)
    #   ]) ++ (with pkgs; []);
    #   multiPkgs = targetPkgs;
    #   profile = "export FHS=1";
    #   runScript = "bash";
    #   extraOutputsToInstall = ["dev"];
    # })
  ];

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = ["qemu:///system"];
      uris = ["qemu:///system"];
    };
  };

  catppuccin.cursors = {
    enable = true;
    accent = "light";
    flavor = "mocha";
  };

  catppuccin.gtk = {
    enable = true;
    flavor = "mocha";
    accent = "red";
    size = "compact";
  };

  gtk = {
    enable = true;
    # catppuccin = {
    #   enable = true;
    #   flavor = "mocha";
    #   accent = "red";
    #   size = "compact";
    # };
    # theme = {
    #   name = "Catppuccin-Mocha-Compact-Red-Dark";
    #   package = pkgs.catppuccin-gtk.override {
    #     colloid-gtk-theme = pkgs.colloid-gtk-theme.overrideAttrs (oldAttrs: {
    #       patches = (oldAttrs.patches or []) ++ [ ./colloid-kicad.patch ];
    #     });
    #     accents = [ "red" ];
    #     size = "compact";
    #     variant = "mocha";
    #   };
    # };

    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "kvantum";
    style.name = "kvantum";
  };

  xdg.enable=true;
  xdg.mime.enable=true;

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    ".xinitrc" = { source = ../dotfiles/.xinitrc; recursive = true; };
    ".xprofile" = { source = ../dotfiles/.xprofile; recursive = true; };
    ".Xresources" = { source = ../dotfiles/.Xresources; recursive = true; };
    ".fehbg" = { source = ../dotfiles/.fehbg; recursive = true; };

    ".config/dunst" = { source = ../dotfiles/.config/dunst; recursive = true; };
    ".config/hypr" = { source = ../dotfiles/.config/hypr; recursive = true; };
    ".config/i3" = { source = ../dotfiles/.config/i3; recursive = true; };
    ".config/macchina" = { source = ../dotfiles/.config/macchina; recursive = true; };
    ".config/picom" = { source = ../dotfiles/.config/picom; recursive = true; };
    ".config/waybar" = { source = ../dotfiles/.config/waybar; recursive = true; };
    ".config/eww" = { source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/nix/dotfiles/.config/eww"; recursive = true; };
    ".config/niri" = { source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/nix/dotfiles/.config/niri"; recursive = true; };

    #".local" = { source = ../dotfiles/.local; recursive = true; };

    "Images" = { source = ../dotfiles/Images; recursive = true; };
    #"suckless" = { source = ../dotfiles/suckless; recursive = true; };

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  programs.bash = {
    enable = true;
    enableCompletion = true;
    initExtra = ''
      PS1='\[\033[31m\][\[\033[32m\]\u\[\033[34m\]@\[\033[34m\]\h \[\033[35m\]\W\[\033[31m\]]$( [ "$IN_NIX_SHELL" == "" ] || echo -n "\[\033[31m\]❬\[\033[34m\]󱄅\[\033[31m\]❭" )\[\033[33m\]󱣀\[\033[37m\] '
      macchina -t Lithium -o processor-load -o memory
    '';
    historyControl = [ "erasedups" "ignoredups" ];
    shellOptions = [ "histappend" ];
    sessionVariables = {
      PROMPT_COMMAND = "history -a; history -c; history -r; $PROMPT_COMMAND";
    };
  };

  # Enable home-manager and git
  programs.home-manager.enable = true;

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "22.11";
}
