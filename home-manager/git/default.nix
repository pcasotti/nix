{
  programs.git = {
    enable = true;
    userName = "Pedro Casotti";
    userEmail = "pedro.c.casotti@gmail.com";
    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = true;
    };
  };
}

