{
  programs.kitty = {
    enable = true;
    environment = {
      "TERM" = "xterm-256color";
    };
    shellIntegration.enableBashIntegration = true;
    shellIntegration.enableFishIntegration = true;
    shellIntegration.enableZshIntegration = true;
    settings = {
      background_opacity = "0.98";
      font_size = "12.0";
      enable_audio_bell = "no";
      confirm_os_window_close = "0";
    };
  };
}
