{
  programs.eza = {
    enable = true;
    extraOptions = [
      "-la"
      "--color=always"
      "--group-directories-first"
    ];
    git = true;
    icons = "auto";
  };
}
