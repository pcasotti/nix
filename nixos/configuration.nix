# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, inputs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  programs.waybar = {
    enable = true;
    #package = inputs.hyprland.packages.${pkgs.system}.waybar-hyprland;
  };

  xdg = {
    autostart.enable = true;
    menus.enable = true;
    mime.enable = true;
    icons.enable = true;

    portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gnome ];
      configPackages = [ pkgs.niri ];
    };
  };

  # Bootloader.
  # boot.zfs.forceImportRoot = false;
  # boot.zfs.allowHibernation = true;
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  #boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.supportedFilesystems = [ "ntfs" "btrfs" ];
  boot.kernelModules = [ "v4l2loopback-dc" "snd-aloop" ];
  boot.kernelParams = [
    "vt.default_red=30,243,166,249,137,245,148,186,88,243,166,249,137,245,148,166"
    "vt.default_grn=30,139,227,226,180,194,226,194,91,139,227,226,180,194,226,173"
    "vt.default_blu=46,168,161,175,250,231,213,222,112,168,161,175,250,231,213,200"
    "quiet"
    "loglevel=3"
    "rd.systemd.show_status=auto"
    "rd.udev.log-priority=3"
    "splash"
  ];
  boot.initrd.systemd.enable = true;
  boot.plymouth = {
    enable = true;
    # themePackages = [ (pkgs.catppuccin-plymouth.override { variant = "mocha"; }) ];
    # theme = "catppuccin-mocha";
  };

  catppuccin.flavor = "mocha";
  catppuccin.enable = true;

  networking.hostName = "box"; # Define your hostname.
  networking.hostId = "d2d98ff0";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  security.pam.services.gtklock = {};
  security.protectKernelImage = false;

  services.logind = {
    lidSwitch = "hibernate";
    suspendKey = "suspend";
    suspendKeyLongPress = "hibernate";
    powerKey = "hibernate";
    powerKeyLongPress = "poweroff";
  };

  #environment.persistence."/persist" = { 
  #  directories = [
  #    "/etc/NetworkManager"
  #    "/var/lib/NetworkManager"
  #  ];
  #  files = [
  #    "/etc/nix/id_rsa"
  #    "/etc/adjtime"
  #  ];
  #};
  #security.sudo.extraConfig = ''
  #  # rollback results in sudo lectures after each reboot
  #  Defaults lecture = never
  #'';
  #boot.initrd.systemd.services.rollback = {
  #  description = "Rollback BTRFS root subvolume to a pristine state";
  #  wantedBy = [
  #    "initrd.target"
  #  ];
  #  after = [
  #    "local-fs.target"
  #  ];
  #  before = [
  #    "sysroot.mount"
  #  ];
  #  unitConfig.DefaultDependencies = "no";
  #  serviceConfig.Type = "oneshot";
  #  serviceConfig.ConditionKernelCommandLine = "!resume";
  #  script = ''
  #    mkdir -p /mnt

  #    # We first mount the btrfs root to /mnt
  #    # so we can manipulate btrfs subvolumes.
  #    mount -t btrfs -o subvol=/ /dev/disk/by-uuid/ce45721e-eb25-4a23-ae96-fc97aeaae182 /mnt

  #    # Save /etc/shadow before wiping root subvolume
  #    mkdir -p /mnt/persist/etc
  #    cp /mnt/root/etc/shadow /mnt/persist/etc/shadow

  #    # While we're tempted to just delete /root and create
  #    # a new snapshot from /root-blank, /root is already
  #    # populated at this point with a number of subvolumes,
  #    # which makes `btrfs subvolume delete` fail.
  #    # So, we remove them first.
  #    #
  #    # /root contains subvolumes:
  #    # - /root/var/lib/portables
  #    # - /root/var/lib/machines
  #    #
  #    # I suspect these are related to systemd-nspawn, but
  #    # since I don't use it I'm not 100% sure.
  #    # Anyhow, deleting these subvolumes hasn't resulted
  #    # in any issues so far, except for fairly
  #    # benign-looking errors from systemd-tmpfiles.
  #    btrfs subvolume list -o /mnt/root |
  #    cut -f9 -d' ' |
  #    while read subvolume; do
  #      echo "deleting /$subvolume subvolume..."
  #      btrfs subvolume delete "/mnt/$subvolume"
  #    done &&
  #    echo "deleting /root subvolume..." &&
  #    btrfs subvolume delete /mnt/root

  #    echo "restoring blank /root subvolume..."
  #    btrfs subvolume snapshot /mnt/root-blank /mnt/root

  #    # Restore /etc/shadow before unmounting
  #    mkdir -p /mnt/root/etc
  #    mv /mnt/persist/etc/shadow /mnt/root/etc/shadow

  #    # Once we're done rolling back to a blank snapshot,
  #    # we can unmount /mnt and continue on the boot process.
  #    umount /mnt
  #  '';
  #};

  services.udev.packages = [ pkgs.qmk-udev-rules ];
  hardware.keyboard.qmk.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  networking.extraHosts = ''
    150.162.66.23 gitlab.labsec.ufsc.br
    150.162.1.49 sistemas.ufsc.br
    150.162.66.50 nexus.labsec.ufsc.br
    150.162.66.24 pbad.labsec.ufsc.br
    54.70.166.11 apis.cff.org.br
    189.84.137.140 servicos.cfm.org.br
    54.70.166.11 apis.cff.org.br
    191.252.191.11 ws.cfo.org.br
  '';

  services.upower.criticalPowerAction = "Hibernate";
  services.power-profiles-daemon.enable = false;
  services.thermald.enable = true;
  services.tlp = {
    enable = true;
    settings = {
      SOUND_POWER_SAVE_ON_AC=1;
      SOUND_POWER_SAVE_ON_BAT=1;
      SOUND_POWER_SAVE_CONTROLLER="Y";

      START_CHARGE_THRESH_BAT0=50;
      STOP_CHARGE_THRESH_BAT0=80;

      CPU_SCALING_GOVERNOR_ON_AC="performance";
      CPU_SCALING_GOVERNOR_ON_BAT="powersave";

      CPU_ENERGY_PERF_POLICY_ON_AC="balance_performance";
      CPU_ENERGY_PERF_POLICY_ON_BAT="balance_power";
    };
  };

  # Set your time zone.
  # time.timeZone = "America/Sao_Paulo";
  services.automatic-timezoned.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "pt_BR.UTF-8";
    LC_IDENTIFICATION = "pt_BR.UTF-8";
    LC_MEASUREMENT = "pt_BR.UTF-8";
    LC_MONETARY = "pt_BR.UTF-8";
    LC_NAME = "pt_BR.UTF-8";
    LC_NUMERIC = "pt_BR.UTF-8";
    LC_PAPER = "pt_BR.UTF-8";
    LC_TELEPHONE = "pt_BR.UTF-8";
    LC_TIME = "pt_BR.UTF-8";
  };

  documentation.dev.enable = true;

  # services.greetd = {
  #   enable = true;
  #   settings = {
  #     default_session = {
  #       command = "${pkgs.dbus}/bin/dbus-run-session ${pkgs.cage}/bin/cage -s -d ${pkgs.greetd.gtkgreet}/bin/gtkgreet";
  #       user = "pedro";
  #     };
  #   };
  # };
  #
  # environment.etc."greetd/environments".text = ''
  #   Hyprland
  #   niri --session
  # '';

  services.desktopManager.cosmic.enable = true;
  services.displayManager.cosmic-greeter.enable = true;

  services.xserver = {
    enable = true;

    displayManager.gdm.enable = false;

    desktopManager = {
      xterm.enable = false;
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };

      gnome.enable = false;
    };

    windowManager.i3 = {
      enable = true;
      package = pkgs.i3-gaps;
      extraPackages = with pkgs; [
        dmenu
        i3lock
        polybar
        alacritty
        picom
        lxappearance
        feh
        unclutter
        #warpd
      ];
    };
  };

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      layout = "";
      variant = "";
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  services.blueman.enable = true;
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio.enable = false;
  hardware.pulseaudio.support32Bit = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.enableAllFirmware = true;
  programs.dconf.enable = true;
  services.pipewire = {
    enable = true;
    pulse.enable = true;
    jack.enable = true;
    alsa.enable = true;
    audio.enable = true;
    wireplumber.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  hardware.graphics.enable = true;
  hardware.graphics.enable32Bit = true;
  hardware.graphics.extraPackages = with pkgs; [
  # hardware.opengl.enable = true;
  # hardware.opengl.driSupport32Bit = true;
  # hardware.opengl.extraPackages = with pkgs; [
    vulkan-tools
    vulkan-loader
    vulkan-headers
    vulkan-validation-layers
    vulkan-extension-layer
  ];

  fonts.packages = with pkgs; [
    font-awesome
    (nerdfonts.override { fonts = [ "DejaVuSansMono" "Iosevka" "IosevkaTerm" ]; })
  ];

  virtualisation.docker.enable = true;
  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;
  # users.groups.libvirtd.members = ["pedro"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.pedro = {
    isNormalUser = true;
    description = "pedro";
    extraGroups = [ "networkmanager" "wheel" "audio" "video" "docker" "libvirtd" "dialout" ];
  };

  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0"
    "python-2.7.18.6"
    "electron-24.8.6"
  ];

  programs.steam.enable = true;
  programs.steam.gamescopeSession.enable = true;
  programs.gamemode.enable = true;

  environment.systemPackages = with pkgs; [
    mangohud

    niri
    # inputs.hyprland.packages.${pkgs.system}.wlroots-hyprland
    # inputs.hyprland.packages.${pkgs.system}.xdg-desktop-portal-hyprland
    # inputs.hyprland.packages.${pkgs.system}.hyprland-protocols

    vim
    wget
    git
    firefox
    chromium
    alacritty
    htop
    onlyoffice-bin
    zip
    unzip
    tmux
    xclip
    wl-clipboard
    openvpn
    networkmanager-openvpn
    libsForQt5.polkit-kde-agent
    libsForQt5.qt5.qtwayland
    pulseaudioFull

    gcc
    clang

    rustc
    cargo

    vulkan-tools
    vulkan-loader

    docker-compose
  ];

  security.polkit.enable = true;
  services.gnome.gnome-keyring.enable = true;

  nix.settings.auto-optimise-store = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}

